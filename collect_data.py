import requests
import re
import csv
from bs4 import BeautifulSoup
from docx import Document
import datetime


def req(url):
    print(url)
    session = requests.Session()
    res = session.get(url, headers={'User-Agent': 'Chrome/78.0.3904.70'})
    soup = BeautifulSoup(res.text, "html.parser")
    if ".kompas." in url:
        article = soup.find("div", {"class": "read__content"})
        title = soup.find("h1", {"class": "read__title"}).get_text()
    elif '.detik.' in url:
        article = soup.find("div", {"id": "detikdetailtext"})
        title = soup.find("div", {"class": "content"}).find("h1").get_text()
    elif '.liputan6.' in url:
        article = soup.find(
            "div", {"class": "article-content-body"})
        article.b.decompose()
        for x in soup.find_all():
            if len(x.get_text(strip=True)) == 0:
                x.decompose()
        for strong in article.find_all("strong"):
            if strong.find(string=re.compile('Simak video menarik berikut ini:')):
                strong.decompose()
        for div in article.find_all("div", {'class': ['article-content-body__item-media', 'article-content-body__item-title', 'article-content-body__item-break', 'read-page--video-gallery--item', 'baca-juga']}):
            div.decompose()
        for h2 in article.find_all("h2", {'class': 'article-content-body__item-title'}):
            h2.decompose()
        title = soup.find("h1", {"class": "entry-title"}).get_text()
    elif '.okezone.' in url:
        article = soup.find("div", {"id": "contentx"})
        article.find("div", {'class': 'detail-tag'}).decompose()
        title = soup.find("h1").get_text()
    else:
        return
    for script in article.find_all("script"):
        script.decompose()
    for style in article.find_all("style"):
        style.decompose()
    for div in article.find_all("div", {'class': ['pic', 'detail_tag', 'photo']}):
        div.decompose()
    for a in article.find_all("a", {'target': '_blank'}):
        a.decompose()
    for strong in article.find_all("strong"):
        if strong.find(string=re.compile('Baca juga|Baca Juga| Simak video menarik berikut ini:')):
            strong.decompose()
    # print(article.get_text().rstrip())
    clean_article = article.get_text().strip().replace('\n', '')
    cleaner_article = re.sub(r'Simak Video.*', '', clean_article)
    # print(clean_article)
    # for line in clean_article:
    #     if re.match(r'^\s*$', line):
    #         print('kosong')
    #         clean_article.remove(line)
    return [url, title, cleaner_article]


def crawl(category, source, max):
  data = []
  day = 1
  date = datetime.datetime(2020, 1, day)
  banjir = 0;
  if category == 'ekonomi_ng':
      if source == 'kompas':
          while(len(data) < max):
              url = 'https://indeks.kompas.com/?site=money&date=' + \
                  date.strftime("%Y-%m-%d")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              for link in soup.find_all('a', {"class": "article__link"}):
                  if(len(data) >= max):
                      break
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "halim" in link.get('href') or "natuna" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)
      elif source == 'detik':
          while(len(data) < max):
              url = 'https://finance.detik.com/indeks?date=' + \
                  date.strftime("%m%%2F%d%%2F%Y")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              content = soup.find('div', {"class": "lf_content"})
              ul = content.find('ul')
              for item in ul.find_all('li'):
                  if(len(data) >= max):
                      break
                  link = item.find('a')
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "halim" in link.get('href') or "natuna" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)
      elif source == 'okezone':
          while(len(data) < max):
              url = 'https://economy.okezone.com/index/' + \
                  date.strftime("%Y/%m/%d")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              ul = soup.find('ul', {"class": "list-berita"})
              for item in ul.find_all('li'):
                  if(len(data) >= max):
                      break
                  link = item.find('a')
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "halim" in link.get('href') or "natuna" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href') or "video" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)

  elif category == 'kesehatan_ng':
      if source == 'liputan6':
          while(len(data) < max):
              url = 'https://www.liputan6.com/health/indeks/' + \
                  date.strftime("%Y/%m/%d")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              for link in soup.find_all('a', {"class": "articles--rows--item__title-link"}):
                  if(len(data) >= max):
                      break
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "corona" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href') or "video" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)
      elif source == 'detik':
          while(len(data) < max):
              url = 'https://health.detik.com/indeks?date=' + \
                  date.strftime("%m%%2F%d%%2F%Y")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              content = soup.find('div', {"class": "rm_content"})
              ul = content.find('ul')
              for item in ul.find_all('li'):
                  if(len(data) >= max):
                      break
                  link = item.find('a')
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "infografis" in link.get('href') or "corona" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)

  elif category == 'pariwisata_ng':
      if source == 'kompas':
          while(len(data) < max):
              url = 'https://indeks.kompas.com/?site=travel&date=' + \
                  date.strftime("%Y-%m-%d")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              for link in soup.find_all('a', {"class": "article__link"}):
                  if(len(data) >= max):
                      break
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "ular" in link.get('href') or "ulat-sagu" in link.get('href') or "ikan-tuna" in link.get('href') or "kopi" in link.get('href') or "kobe" in link.get('href') or "corona" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href') or "photos" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)
      elif source == 'detik':
          while(len(data) < max):
              url = 'https://travel.detik.com/indeks?date=' + \
                  date.strftime("%m%%2F%d%%2F%Y")
              res = requests.get(url)
              soup = BeautifulSoup(res.text, "html.parser")
              content = soup.find('div', {"class": "content"})
              ul = content.find('section', {"class": "list__news"})
              for item in ul.find_all('article'):
                  if(len(data) >= max):
                      break
                  link = item.find('a')
                  if "banjir" in link.get('href'):
                      banjir += 1
                      if(banjir > 7):
                          continue
                  if "ular" in link.get('href') or "ulat-sagu" in link.get('href') or "ikan-tuna" in link.get('href') or "kopi" in link.get('href') or "kobe" in link.get('href') or "corona" in link.get('href') or "foto" in link.get('href') or "tv" in link.get('href') or "detiktravel" in link.get('href'):
                      continue
                  else:
                      data.append([link.get('href'), '', ''])
              day += 1
              date = datetime.datetime(2020, 1, day)
  for i, item in enumerate(data):
      data[i] = req(item[0])
  return data


# list_ekonomi = []
# list_url_ekonomi = []
# list_kesehatan = []
# list_url_kesehatan = []
# list_pariwisata = []
# list_url_pariwisata = []

# f = open('./data/url_ekonomi.txt', 'r')
# data = f.readlines()
# f.close()
# docs = Document()
# for index, url in enumerate(data):
#     if index in [51, 61]:
#         print(index)
#         print(url)
#         data = req(url)
#         print(f'judul : {data[0]}')
#         # docs.add_heading(data[0])
#         # docs.add_paragraph(data[1])
#         f = open(f'./data/training/ekonomi/{index}.txt', 'w')
#         f.write(data[1])
#         f.close
# docs.save('data_pariwisata.docx')
# with open('data_pariwisata.csv', mode='w') as data_kesehatan:
#     employee_writer = csv.writer(data_kesehatan)
#     for index, url in enumerate(data):
#         print(index)
#         print(url)
#         data = req(url)
#         list_ekonomi.append(data)
#     employee_writer.writerow(list_ekonomi)
#     data_kesehatan.close()

docs = Document()

data = crawl('ekonomi_ng', 'detik', 75) + crawl('ekonomi_ng', 'kompas', 80) + crawl('ekonomi_ng', 'okezone', 75)


url_data = open('./data/new/url_ekonomi_ng.txt', 'w')
for index, url in enumerate(data):
    text = data[index][2]
    print('processing:')
    print(data[index][0])
    docs.add_heading(data[index][1])
    docs.add_paragraph(data[index][0])
    docs.add_paragraph(text)
    f = open(
        f'./data/new/training/ekonomi lainnya/{index}.txt', 'w', encoding="utf-8")
    f.write(text)
    f.close
    url_data.write(data[index][0])
    url_data.write('\n')
url_data.close
docs.save('./data/new/data_ekonomi_ng.docx')

# f = open('./data/new/url_pariwisata_ng_lombok.txt', 'r')
# url_data = f.readlines()
# f.close

# docs = Document()

# for index, url in enumerate(url_data):
#     name = 210 + index
#     data = req(url)
#     text = data[2]
#     print(data[0])
#     docs.add_heading(data[1])
#     docs.add_paragraph(data[0])
#     docs.add_paragraph(text)
#     f = open(
#         f'./data/new/training/pariwisata lainnya/{name}.txt', 'w', encoding="utf-8")
#     f.write(text)
#     f.close
# docs.save('./data/new/data_pariwisata_ng_lombok.docx')