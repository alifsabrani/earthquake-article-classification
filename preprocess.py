from nltk.tokenize import RegexpTokenizer
from nltk import FreqDist
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.datasets import load_files
import matplotlib.pyplot as plt
import numpy
import glob
import csv
import math


def tokenize(document):
    stemmer_factory = StemmerFactory()
    stopword_factory = StopWordRemoverFactory()

    stopword = stopword_factory.create_stop_word_remover()
    stemmer = stemmer_factory.create_stemmer()
    tokenizer = RegexpTokenizer(r'[a-zA-Z]+')

    token = tokenizer.tokenize(document.lower())
    stemmed = stemmer.stem(', '.join(token))
    sw_removed = stopword.remove(stemmed).split(' ')
    bigram = []
    for i in range(len(sw_removed) - 1):
        bigram.append(sw_removed[i] + ' ' + sw_removed[i+1])
    # return ', '.join(token)
    return ', '.join(bigram)


def corpus(path):
    file_list = glob.glob(path+'/*.txt')
    corpus = {}
    for file_path in file_list:
        with open(file_path) as f_input:
            corpus[file_path] = f_input.read()
    return corpus


def export_csv(filename, vector, feature):
    with open(filename, mode='w') as data:
        writer = csv.writer(data)
        writer.writerow(feature)
        writer.writerows(vector)
        data.close()


def idf(corpus, vocabulary):
    df = {}
    idf = {}
    for term in vocabulary:
        df[term] = 0
        for document in corpus:
            if document[vocabulary[term]] > 0:
                # print(f'kata {term} ada di dokumen {document}')
                df[term] += 1
        idf[term] = math.log((len(corpus)/df[term]), 10)
    return idf


# stemmer_factory = StemmerFactory()
# tfidf_transformer = TfidfTransformer(norm="none")
# stopword_factory = StopWordRemoverFactory()

# stemmer = stemmer_factory.create_stemmer()
# tokenizer = RegexpTokenizer(r'[a-zA-Z]+')
# stopword = stopword_factory.get_stop_words()
# print(stopword)
# count_vectorizer = CountVectorizer(
#     tokenizer=tokenize, analyzer='word', stop_words=stopword)
# freq = count_vectorizer.fit_transform(corpus('./data/training/pariwisata'))
# print(count_vectorizer.vocabulary_)
# print(idf(freq.toarray(), count_vectorizer.vocabulary_))
# export_csv('data_100_pariwisata.csv', freq.toarray(),
#            count_vectorizer.get_feature_names())
# print(numpy.sort(freq.toarray()))
# freq_table = plt.table(rowLabels=count_vectorizer.get_feature_names(), colLabels=[
#     'Ekonomi', 'Kesehatan', 'Pariwisata'], cellText=numpy.transpose(freq.toarray()), loc="center")
# plt.show()

# tfidf_vectorizer = TfidfVectorizer(
#     norm=None, tokenizer=tokenize, smooth_idf=False, analyzer='word', sublinear_tf=False)
# tfidf = tfidf_vectorizer.fit_transform(corpus('./data/training'))
# print(tfidf.toarray())
# export_csv('data_tfidf.csv', tfidf.toarray(),
#            tfidf_vectorizer.get_feature_names())

# preprocessor(artikel_ekonomi.read())

# token_kesehatan = tokenizer.tokenize(artikel_kesehatan.read().lower())
# token_ekonomi = tokenizer.tokenize(artikel_ekonomi.read().lower())
# token_pariwisata = tokenizer.tokenize(artikel_pariwisata.read().lower())
# token_testing = tokenizer.tokenize(artikel_testing.read().lower())

# stemmed_kesehatan = stopword.remove(
#     stemmer.stem(', '.join(token_kesehatan)))
# stemmed_ekonomi = stopword.remove(stemmer.stem(', '.join(token_ekonomi)))
# stemmed_pariwisata = stopword.remove(
#     stemmer.stem(', '.join(token_pariwisata)))
# stemmed_testing = stopword.remove(stemmer.stem(', '.join(token_testing)))

# freq_kesehatan = FreqDist(stemmed_kesehatan.split(' '))
# freq_ekonomi = FreqDist(stemmed_ekonomi.split(' '))
# freq_pariwisata = FreqDist(stemmed_pariwisata.split(' '))

# print(freq_kesehatan.most_common(10))
# print(freq_ekonomi.most_common(10))
# print(freq_pariwisata.most_common(10))
# print(', '.join(stemmed_kesehatan.split(' ')))
# print(', '.join(stemmed_ekonomi.split(' ')))
# print(', '.join(stemmed_pariwisata.split(' ')))
# print(', '.join(stemmed_testing.split(' ')))

# EXAMPLE PROCESS
example_corpus = corpus("./data/example/testing")
for key, value in example_corpus.items():
    tokenized = tokenize(value)
    print(key, tokenized)
