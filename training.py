from nltk.tokenize import RegexpTokenizer
from nltk import FreqDist
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import numpy
import glob
import csv
import math
import pickle


def tokenize(document):
    stemmer_factory = StemmerFactory()
    stopword_factory = StopWordRemoverFactory()

    stopword = stopword_factory.create_stop_word_remover()
    stemmer = stemmer_factory.create_stemmer()
    tokenizer = RegexpTokenizer(r'[a-zA-Z]+')

    token = tokenizer.tokenize(document.lower())
    stemmed = stemmer.stem(', '.join(token))
    sw_removed = stopword.remove(stemmed).split(' ')
    return sw_removed


def get_corpus(path):
    file_list = glob.glob(path+'/*.txt')
    corpus = []
    for file_path in file_list:
        if 'lainnya' in path:
            with open(file_path, 'r', encoding='utf-8') as f_input:
                corpus.append(f_input.read())
        else:
            with open(file_path, 'r') as f_input:
                corpus.append(f_input.read())
    return corpus


def export_csv(filename, vector, feature):
    print(feature)
    with open(filename, mode='w') as data:
        writer = csv.writer(data)
        writer.writerow(feature)
        writer.writerows(vector)
        data.close()


def count_word_probability(documents, vocabulary, total_freq):
    word_probs = []
    tct = []
    for i in range(len(vocabulary)):
        freq = 0
        for doc in documents:
            freq = freq + doc[i]
        tct.append(freq)
    for tf in tct:
        word_probs.append((tf + 1) / (total_freq + len(vocabulary)))
    return word_probs


def get_idf(corpus, vocabulary):
    df = {}
    idf = {}
    for term in vocabulary:
        df[term] = 0
        for document in corpus:
            if document[vocabulary.index(term)] > 0:
                # print(f'kata {term} ada di dokumen {document}')
                df[term] = df[term] + 1
        idf[term] = math.log((len(corpus)/df[term]), 10)
    return idf.items()


def tf_idf(tf, idf):
    tfidf = []
    for category in tf:
        tf_idf_cat = []
        for doc in category:
            tf_idf_doc = []
            for i, feature in enumerate(doc):
                tf_idf_doc.append(feature * idf[i])
            tf_idf_cat.append(tf_idf_doc)
        tfidf.append(tf_idf_cat)
    return tfidf


def train(vector, vocabulary):
    corpus_size = len(vector[0]) + len(vector[1]) + len(vector[2])
    prior_probability = []
    word_probability = []
    total_word_freq = []
    for category in vector:
        total = 0
        for row in category:
            total = total + sum(row)
        total_word_freq.append(total)
    for i, category in enumerate(vector):
        prior_probability.append(len(category) / corpus_size)
        word_probability.append(count_word_probability(
            category, vocabulary, total_word_freq[i]))
    # pickle.dump(prior_probability, open('prior_probability.sav', 'wb'))
    pickle.dump(word_probability, open('word_probability.sav', 'wb'))


def classify(document, prior_probability, word_probability):
    probs = []
    for i, category in enumerate(prior_probability):
        sum_word_probability = 0
        for j, tf in enumerate(document[0]):
            if tf > 0:
                for k in range(tf):
                    sum_word_probability = sum_word_probability + \
                        math.log10(word_probability[i][j])
        probs.append(math.log10(category) + sum_word_probability)
    print(probs)


stopword_factory = StopWordRemoverFactory()

stopword = stopword_factory.get_stop_words()
corpus = get_corpus('./data/training/ekonomi gempa') + \
    get_corpus('./data/training/kesehatan gempa') + \
    get_corpus('./data/training/pariwisata gempa') + \
    get_corpus('./data/training/ekonomi lainnya') + \
    get_corpus('./data/training/kesehatan lainnya') + \
    get_corpus('./data/training/pariwisata lainnya')
example_test_doc = []

with open('./data/example/testing/example_testing.txt') as f_input:
    example_test_doc.append(f_input.read())

example_corpus = get_corpus('./data/example')
count_vectorizer = CountVectorizer(
    tokenizer=tokenize, analyzer='word', ngram_range=(1, 2))
tf = count_vectorizer.fit_transform(corpus)
idf = get_idf(tf.toarray(), count_vectorizer.get_feature_names())
# test_example = count_vectorizer.fit_transform(
#     example_test_doc)

# export_csv('./data/example/feature_test_example.csv', tf_example.toarray(),
#            count_vectorizer.get_feature_names())

# export_csv('./data/example/example_idf_uni_bi.csv', idf(tf_example.toarray(), count_vectorizer.get_feature_names()),
#            count_vectorizer.get_feature_names())
# count_vectorizer = pickle.load(open('count_vectorizer.sav', 'rb'))

tf_ekonomi_g = count_vectorizer.transform(
    get_corpus('./data/training/ekonomi gempa'))
tf_kesehatan_g = count_vectorizer.transform(
    get_corpus('./data/training/kesehatan gempa'))
tf_pariwisata_g = count_vectorizer.transform(
    get_corpus('./data/training/pariwisata gempa'))
tf_ekonomi_ng = count_vectorizer.transform(
    get_corpus('./data/training/ekonomi lainnya'))
tf_kesehatan_ng = count_vectorizer.transform(
    get_corpus('./data/training/kesehatan lainnya'))
tf_pariwisata_ng = count_vectorizer.transform(
    get_corpus('./data/training/pariwisata lainnya'))
print(type(tf_ekonomi_g.toarray()))
vector = [tf_idf(tf_ekonomi_g.toarray(), idf), tf_idf(tf_kesehatan_g.toarray(), idf),
          tf_idf(tf_pariwisata_g.toarray(), idf), tf_idf(
              tf_ekonomi_ng.toarray(), idf), tf_idf(tf_kesehatan_ng.toarray(), idf),
          tf_idf(tf_pariwisata_ng.toarray(), idf)]

pickle.dump(vector, open('vector.sav', 'wb'))
# vector = pickle.load(open('vector.sav', 'rb'))

# word_probability = pickle.load(open('word_probability.sav', 'rb'))
# x = word_probability[0][0]
# print(type(x))
# with open('vector_ptc_ekonomi', mode='w') as data:
#     writer = csv.writer(data)
#     writer.writerow(count_vectorizer.get_feature_names())
#     writer.writerow(word_probability[0])
#     data.close()
# train(vector, count_vectorizer.vocabulary_)

# test = open('./data/example_testing.txt', 'r')
# tf = count_vectorizer.transform(test)
# prior_probability = pickle.load(open('prior_probability.sav', 'rb'))
# word_probability = pickle.load(open('word_probability.sav', 'rb'))
# classify(tf.toarray(), prior_probability, word_probability)
